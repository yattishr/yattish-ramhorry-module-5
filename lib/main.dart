import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: 'AIzaSyB8-k-BmYh8zWEWYoNGuIGy3KIP7Bfh8cM',
          appId: '1:106774497746:android:863650b5498bc146ece00b',
          messagingSenderId: '106774497746',
          projectId: 'mtn-module-5-8f388'));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'MTN Module 5 Assessment';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

void myFavoriteApp(firstName, lastName, favoriteApp, city) {
  FirebaseFirestore.instance
      .collection('myFavoriteApp')
      .add({
        'firstName': firstName,
        'lastName': lastName,
        'favoriteApp': favoriteApp,
        'city': city
      })
      .then((value) => print('Favorite App added...'))
      .catchError((error) => print("Unable to add record: $error"));
}

//Define a custom form widget
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Define a corresponding State class.
// This class holds data related to the form
class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  final myController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final favoriteAppController = TextEditingController();
  final cityController = TextEditingController();

  @override
  void dispose() {
    firstNameController.dispose();
    lastNameController.dispose();
    favoriteAppController.dispose();
    cityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              autofocus: true,
              controller: firstNameController,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your first name';
                }
                return null;
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Your First name',
              ),
            ),
            TextFormField(
              controller: lastNameController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your last name';
                }
                return null;
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Your Last name',
              ),
            ),
            TextFormField(
              controller: favoriteAppController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your favorite app';
                }
                return null;
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Your Favorite app',
              ),
            ),
            TextFormField(
              controller: cityController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your City';
                }
                return null;
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Your City',
              ),
            ),
            ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Processing Data...')),
                  );
                  myFavoriteApp(
                      firstNameController.text,
                      lastNameController.text,
                      favoriteAppController.text,
                      cityController.text);
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Added successfully...')),
                  );
                  firstNameController.text = "";
                  lastNameController.text = "";
                  favoriteAppController.text = "";
                  cityController.text = "";
                }
              },
              child: const Text('Submit'),
            ),
          ],
        ));
  }
}
